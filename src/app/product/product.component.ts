import { Component, OnInit } from '@angular/core';
import { ProductInfo } from '../common/catalog-service/catalog.types';
import { CatalogService } from '../common/catalog-service/catalog.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ConfigService } from '../common/config-service/config.service';
import { BasketService } from '../basket/basket.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  product: ProductInfo = null;

  constructor(
    private catalogService: CatalogService,
    private route: ActivatedRoute,
    private config: ConfigService,
    private basketService: BasketService,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.catalogService.getProductById(Number(params.get('id'))))
    ).subscribe((res: ProductInfo) => {
      this.product = res;
    });
  }

  addToBasket(productId: number): void {
    this.basketService.addProduct(productId);
  }

  goToBasket(): void {
    this.router.navigate(['/basket']);
  }
}
