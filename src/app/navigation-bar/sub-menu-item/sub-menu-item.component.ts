import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CategoryNode } from 'src/app/common/catalog-service/catalog.types';

@Component({
  selector: 'app-sub-menu-item',
  templateUrl: './sub-menu-item.component.html',
  styleUrls: ['./sub-menu-item.component.scss']
})
export class SubMenuItemComponent implements OnInit {

  @Input() category: CategoryNode;
  @Output() onClick = new EventEmitter<CategoryNode>();

  constructor() { }

  ngOnInit() {
  }

  clickToItem(category: CategoryNode): void {
    this.onClick.emit(category);
  }
}
