import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CategoryNode } from 'src/app/common/catalog-service/catalog.types';

@Component({
  selector: 'app-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() category: CategoryNode;
  @Output() onClick = new EventEmitter<CategoryNode>();

  constructor() { }

  ngOnInit() {
  }

  clickToItem(category: CategoryNode): void {
    this.onClick.emit(category);
  }
}
