import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { CatalogService } from '../common/catalog-service/catalog.service';
import { CategoryNode } from '../common/catalog-service/catalog.types';
import { Router } from '@angular/router';
import { AuthService } from '../common/auth-service/auth.service';
import { User } from '../models/User';
import { BasketService } from '../basket/basket.service';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent implements OnInit {

  categories: CategoryNode[] = null;
  currentUser: User = null;

  @Output() selectCategory = new EventEmitter<CategoryNode>();

  constructor(
    private catalog: CatalogService,
    private router: Router,
    private authService: AuthService,
    private basketService: BasketService
  ) {
    this.authService.currentUser.subscribe(res => {
      this.currentUser = res;
    });
  }

  ngOnInit() {
    this.catalog.categoriesTree.subscribe(res => {
      this.categories = res;
    });
  }

  clickByMenuItem(category: CategoryNode): void {
    if (category.parentCategoryId !== -1) {
      this.router.navigate([`catalog/${category.id}`]);
    }
  }

  logout(): void {
    this.authService.logout();
  }

  goToBasketPage(): void {
    this.router.navigate(['basket']);
  }
}
