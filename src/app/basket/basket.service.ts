import { Injectable, OnInit } from '@angular/core';
import { AuthService } from '../common/auth-service/auth.service';
import { Product } from './basket.types';
import { CatalogService } from '../common/catalog-service/catalog.service';
import { ProductInfo } from '../common/catalog-service/catalog.types';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BasketService implements OnInit {

  private productsSubject: BehaviorSubject<Product[]>;
  public productsObservable: Observable<Product[]>;

  constructor(
    private authService: AuthService,
    private catalog: CatalogService
  ) {
    const initProducts: Product[] = JSON.parse(localStorage.getItem(`${this.authService.currentUserValue.login}-basketProducts`));
    this.productsSubject = new BehaviorSubject<Product[]>(initProducts ? initProducts : []);
    this.productsObservable = this.productsSubject.asObservable();
  }

  ngOnInit(): void {}

  public get products(): Product[] {
    return this.productsSubject.value;
  }

  public get productsCount(): number {
    return this.products.reduce((acc, x) => acc + x.count, 0);
  }

  public get productsPrice(): number {
    return this.products.reduce((acc, x) => acc + x.price * x.count, 0);
  }

  public productInBasket(productId: number): boolean {
    return this.products.findIndex(p => p.id === productId) !== -1;
  }

  addProduct(productId: number): void {
    const productsLocal = this.products;
    const product: Product = productsLocal.find(p => p.id === productId);
    if (product) {
      product.count++;
    } else {
      this.catalog.getProductById(productId).subscribe((res: ProductInfo) => {
        productsLocal.push({
          id: res.id,
          name: res.title,
          price: res.price,
          categoryId: res.categoryId,
          count: 1
        });
        this.productsSubject.next(productsLocal);
        localStorage.setItem(`${this.authService.currentUserValue.login}-basketProducts`, JSON.stringify(productsLocal));
      });
    }
    this.productsSubject.next(productsLocal);
    localStorage.setItem(`${this.authService.currentUserValue.login}-basketProducts`, JSON.stringify(productsLocal));
  }

  removeProduct(productId: number): void {
    const productsLocal = this.products;
    const product: Product = productsLocal.find(p => p.id === productId);
    if (product) {
      product.count--;
      if (product.count === 0) {
        const productIndex: number = this.products.indexOf(product);
        productsLocal.splice(productIndex, 1); 
      }
    }
    this.productsSubject.next(productsLocal);
    localStorage.setItem(`${this.authService.currentUserValue.login}-basketProducts`, JSON.stringify(productsLocal));
  }
}
