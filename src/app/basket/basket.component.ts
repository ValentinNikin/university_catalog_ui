import { Component, OnInit } from '@angular/core';
import { Product } from './basket.types';
import { BasketService } from './basket.service';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {

  products: Product[] = [];

  constructor(
    private basketService: BasketService
  ) { }

  ngOnInit() {
    
  }

  removeProduct(productId: number): void {
    this.basketService.removeProduct(productId);
  }

  addProduct(productId: number): void {
    this.basketService.addProduct(productId);
  }
}
