import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { CategoryInfo } from 'src/app/common/catalog-service/catalog.types';
import { CatalogService } from 'src/app/common/catalog-service/catalog.service';
import { ConfigService } from 'src/app/common/config-service/config.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-child-categories',
  templateUrl: './child-categories.component.html',
  styleUrls: ['./child-categories.component.scss']
})
export class ChildCategoriesComponent implements OnInit {

  @Input() categoryId: any = -1;

  categories: CategoryInfo[] = [];

  constructor(
    private catalog: CatalogService,
    private config: ConfigService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.categoryId) {
      console.log(changes.categoryId);
      this.loadInfoAboutCategories();
    }
  }

  loadInfoAboutCategories(): void {
    if (this.categoryId === -1) {
      this.catalog.getMainCategories().subscribe(res => {
        this.categories = res;
      });
    } else {
      this.catalog.getChildCategories(this.categoryId).subscribe(res => {
        this.categories = res;
      }); 
    }
  }

  showChildCategories(id: number): void {
    this.router.navigate([`catalog/${id}`]);
  }
}
