import { Component, OnInit, EventEmitter, Input, SimpleChanges, OnChanges, ViewChild } from '@angular/core';
import { CategoryNode, CategoryInfo } from 'src/app/common/catalog-service/catalog.types';
import { CatalogService } from 'src/app/common/catalog-service/catalog.service';
import { switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Route, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit, OnChanges {

  @ViewChild('appProducts', {static: false}) appProducts: any;
  @ViewChild('appChildCategories', {static: false}) appChildCategories: any;

  categoryInfo: CategoryInfo = null;
  productsCategory: boolean = true;

  constructor(
    private catalogService: CatalogService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    console.log('onInit');
    this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.catalogService.getCategory(Number(params.get('id'))))
    ).subscribe((res: CategoryInfo) => {
      this.categoryInfo = res;
      this.productsCategory = this.categoryInfo.productsCategory;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {

  }

  categoryChanged(): void {
    
  }
}
