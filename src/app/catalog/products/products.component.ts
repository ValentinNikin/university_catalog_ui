import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CatalogService } from 'src/app/common/catalog-service/catalog.service';
import { ProductShortInfo } from 'src/app/common/catalog-service/catalog.types';
import { ConfigService } from 'src/app/common/config-service/config.service';
import { Router } from '@angular/router';
import { BasketService } from 'src/app/basket/basket.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnChanges {

  @Input() categoryId: any = -1;

  products: ProductShortInfo[] = [];

  constructor(
    private catalog: CatalogService,
    private config: ConfigService,
    private router: Router,
    private basketService: BasketService
  ) { }

  ngOnInit() {
    console.log('ngOnInit');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.categoryId) {
      console.log(changes.categoryId);
      this.loadInfoAboutProducts();
    }
  }

  loadInfoAboutProducts(): void {
    this.catalog.getProductsFromCategory(this.categoryId).subscribe(res => {
      this.products = res;
      console.log(res);
    })
  }

  showProductInfo(id: number): void {
    this.router.navigate([`product/${id}`]);
  }

  addProductToBasket(productId: number): void {
    this.basketService.addProduct(productId);
  }

  goToBasket(): void {
    this.router.navigate(['/basket']);
  }
}
