import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as AppIcons from './app-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { CatalogService } from './common/catalog-service/catalog.service';
import { MenuItemComponent } from './navigation-bar/menu-item/menu-item.component';
import { SubMenuItemComponent } from './navigation-bar/sub-menu-item/sub-menu-item.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { CatalogComponent } from './catalog/catalog.component';
import { ChildCategoriesComponent } from './catalog/child-categories/child-categories.component';
import { ProductsComponent } from './catalog/products/products.component';
import { ProductComponent } from './product/product.component';
import { BasketComponent } from './basket/basket.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    MenuItemComponent,
    SubMenuItemComponent,
    NavigationBarComponent,
    FooterComponent,
    CatalogComponent,
    MainComponent,
    ChildCategoriesComponent,
    ProductsComponent,
    ProductComponent,
    BasketComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule
  ],
  providers: [
    CatalogService,
    BasketComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    for (const icon of Object.keys(AppIcons)) {
      library.addIcons(AppIcons[icon]);
    }
  }
}
