import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map, take } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.authService.currentUser.pipe(
            map((res) => {
                console.log('canActivate');
                console.log(res);
                if (!res) {
                    this.router.navigate(['/login']);
                    console.log('false');
                    return false;
                }

                console.log('true');
                return true;
            }),
            take(1)
        );
    }
}