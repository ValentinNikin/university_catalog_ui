import { Binary } from '@angular/compiler';

export interface CategoryNode {
    id: number;
    title: string;
    parentCategoryId: number;
    childCategories: CategoryNode[]
};

export interface CategoryInfo {
    id: number;
    title: string;
    description: string;
    productsCategory: boolean;
    imagePath: string;
}

export interface ProductShortInfo {
    id: number;
    title: string;
    imagePath: string;
    categoryId: number;
    price: number;
}

export interface ProductInfo extends ProductShortInfo {
    description: string;
    maker: string;
    makeDate: string;
}