import { Injectable, OnInit } from '@angular/core';
import { ConfigService } from '../config-service/config.service';
import { CategoryNode, CategoryInfo, ProductShortInfo, ProductInfo } from './catalog.types';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  categoriesTreeSource = new BehaviorSubject<CategoryNode[]>(null);
  categoriesTree: Observable<CategoryNode[]> = this.categoriesTreeSource.asObservable();

  constructor(
    private configService: ConfigService,
    private http: HttpClient
  ) {
    this.loadAllCategories();
  }

  loadAllCategories(): void {
    this.http.get(`${this.configService.apiUrl}/catalog/categories`)
      .pipe()
      .subscribe((res: CategoryNode[]) => {
        this.categoriesTreeSource.next(res);
      }, error => {
        console.log(`Get categories tree error: ${JSON.stringify(error)}`);
      });
  }

  getCategory(id: number): Observable<CategoryInfo> {
    return this.http.get(`${this.configService.apiUrl}/catalog/categories/${id}`)
      .pipe(map(this.extractGetCategoryResponse.bind(this)));
  }

  getMainCategories(): Observable<CategoryInfo[]> {
    return this.http.get(`${this.configService.apiUrl}/catalog/mainCategories`)
      .pipe(map(this.extractGetCategoriesResponse.bind(this)))
  }

  getChildCategories(id: number): Observable<CategoryInfo[]> {
    return this.http.get(`${this.configService.apiUrl}/catalog/childCategories/${id}`)
      .pipe(map(this.extractGetCategoriesResponse.bind(this)))
  }

  getProductsFromCategory(id: number): Observable<ProductShortInfo[]> {
    return this.http.get(`${this.configService.apiUrl}/catalog/productsFromCategory/${id}`)
      .pipe(map(this.extractGetProductsFromCategoryResponse.bind(this)));
  }

  getProductById(id: number): Observable<ProductInfo> {
    return this.http.get(`${this.configService.apiUrl}/catalog/products/${id}`)
      .pipe(map(this.extractGetProductByIdResponse.bind(this)));
  }

  extractGetCategoryResponse(res: CategoryInfo): CategoryInfo {
    return res;
  }

  extractGetCategoriesResponse(res: CategoryInfo[]): CategoryInfo[] {
    return res;
  }

  extractGetProductsFromCategoryResponse(res: ProductShortInfo[]): ProductShortInfo[] {
    return res;
  }

  extractGetProductByIdResponse(res: ProductInfo): ProductInfo {
    return res;
  }
}
