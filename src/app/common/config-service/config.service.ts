import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  apiUrl: string = 'http://localhost:4000/api';
  imagesUrl: string = "http://localhost:4000/images";

  constructor() { }
}
